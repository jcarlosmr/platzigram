const yo = require('yo-yo')
const translate = require('../translate')

module.exports = function pictCards(pic) {
    let el=yo`<div><img class="user-picture" src="/${pic.url}" /></div>`
    return el
}
