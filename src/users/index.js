const yo = require('yo-yo')
const page = require('page')
const title = require('title')
const empty = require('empty-element')
const header = require('../header')
const sa = require('superagent')
const template = require('./templateUserInfo')

page('/user/:username', header, getUsersMidd, getUserPict, function (ctx, next) {
    title('Platzigram - User')
    const main = document.getElementById('main-container')
    empty(main).appendChild(template(ctx.user))
})  

function getUsersMidd(ctx, next) {
    sa
        .get('/api/user/' + ctx.params.username)
        .end((err, res) => {
            if (err) return console.log(err)
            ctx.user = res.body
            next()
        })
}

function getUserPict(ctx, next) {
    sa
        .get('/api/pictures/' + ctx.params.username)
        .end((err, res) => {
            if (err) return console.log(err)
            ctx.user.pictures = res.body
            next()
        })
}