const yo = require('yo-yo')
const landing = require('../landing')
const translate = require('../translate')
const pict = require('./templatePicCard')

module.exports = function (user) {
    const el = yo`<div class="container user-data">
        <div class="row">
            <div class="col s12 m10 offset-m1">
                <div class="user-info">
                    <img class="info-avatar" src="/${user.avatar}" alt="">
                    <span class="username">${user.fullname}</span>                
                    <p>
                        ${translate.message('about-me')}
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m10 offset-m1 pool-images">
                ${user.pictures.map((p) => pict(p))}
            </div>
        </div>
    </div>`

    return el
}