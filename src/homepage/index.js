const page = require('page')
const empty = require('empty-element')
const template = require('./template')
const title = require('title')
const sa = require('superagent')
const headerMiddleware = require('../header')
const axios = require('axios')
const spinner = require('../spinner')

page('/', headerMiddleware, setSpinner, loadPictureAsync, function (ctx, next) {
    title('Playzigram')
    const main = document.getElementById('main-container')
    empty(main).appendChild(template(ctx.pictures))
})

function setSpinner(ctx, next) {
    const main = document.getElementById('main-container')
    empty(main).appendChild(spinner())
    next()
}

/**
 * Usando diferentes métodos para realizar llamados a una api
 * SuperAgent
 * axios
 * fetch (ya viene incluida en el navegador)
 */
function loadPicture(ctx, next) {
    sa
        .get('/api/pictures')
        .end(function (err, res) {
            if (err) return console.log(err)
            ctx.pictures = res.body
            next()
        })
}

function loadPictureAxios(ctx, next) {
    axios.get('/api/pictures')
        .then((res) => {
            ctx.pictures = res.data
            next()
        })
        .catch((err) => console.log(err))
}

function loadPictureFetch(ctx, next) {
    fetch('/api/pictures')
        .then((res) => res.json())
        .then((res) => {
            ctx.pictures = res
            next();
        })
        .catch((err) => console.log(err))
}

async function loadPictureAsync(ctx, next) {
    try {
        ctx.pictures = await fetch('/api/pictures').then((res) => res.json())
        next()
    } catch (err) {
        return console.log(err)
    }
}