const yo = require('yo-yo')
const layout = require('../layout')
const picture = require('../picture-card')
const translate = require('../translate').message //como se va a usa solo este metodo, entonces se puede indicar directamente
const sa = require('superagent')

module.exports = function (pictures) {
    const el = yo`<div id="data-pict" class="conttainer timeline">
            <div class="row">
                <div class="col s12 m10 offset-m1 l8 offset-l2 center-align">
                    <form enctype="multipart/form-data" class="form-upload" id="formUpload" onsubmit=${onsubmit} >
                        <div id="fileName" class="file-upload btn btn-flat cyan">
                            <span><i class="fas fa-camera-retro"></i> ${translate('upload-picture')}</span>
                            <input name="picture" id="file" type="file" class="upload" onchange=${onchange} />
                        </div>
                        <button id="btnUpload" type="submit" class="btn btn-flas cyan hide">${translate('upload')}</button>
                        <button id="btnCancel" type="button" class="btn btn-flas red hide" onclick=${cancel}><i class="fas fa-times"></i></button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m10 offset-m1 l6 offset-l3">
                    ${pictures.map(function (pic) {
                        return picture(pic)
                    })}
                </div>    
            </div>
        </div>`

    function onsubmit(ev) {
        ev.preventDefault()
        const data = new FormData(this)

        sa
            .post('/api/pictures')
            .send(data)
            .end((err, res) => {
                if (err) return console.log(err)
                console.log(res)
                toggleButtons()
                document.getElementById('formUpload').reset()
            })
    }

    function toogleSpinner() {
        document.getElementById('loader').classList.toggle('hide')
    }
    
    function toggleButtons() {
        document.getElementById('fileName').classList.toggle('hide')
        document.getElementById('btnUpload').classList.toggle('hide')
        document.getElementById('btnCancel').classList.toggle('hide')
    }

    function cancel() {
        toggleButtons()
        document.getElementById('formUpload').reset()
    }

    function onchange() {
        toggleButtons()        
    }

    return layout(el)
}
