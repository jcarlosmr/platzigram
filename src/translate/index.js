if (!window.Intl) {
    window.Intl = require('intl')
    require('intl/locale-data/jsonp/en-US.js')
    require('intl/locale-data/jsonp/es.js')
}

window.IntlRelativeFormat = require('intl-relativeformat')
window.IntlMessageFormat = require('intl-messageformat')

require('intl-relativeformat/dist/locale-data/en.js')
require('intl-relativeformat/dist/locale-data/es.js')
//require('intl-messageformat/intl-messageformat.min.js')

const rf = new IntlRelativeFormat('es')

const es = require('./es')
const en = require('./en-US')

let MESSAGES = {}
MESSAGES.es = es
MESSAGES['en-US'] = en

const locale = localStorage.locale || 'es'

function message(mes, opts) {
    opts = opts || {}
    const msg = new IntlMessageFormat(MESSAGES[locale][mes], locale, null)
    return msg.format(opts)
}

function date(d) {
    return new IntlRelativeFormat(locale).format(d)
}

module.exports = {
    message: message,
    date: date
}