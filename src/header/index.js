const yo = require('yo-yo')
const translate = require('../translate')
const empty = require('empty-element')

const el = yo`<nav class="header">
        <div class="nav-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 offset-m1">
                        <a href="/" class="brand-logo platzigram">Platzigram</a>
                    </div>
                    <div class="col s2 m6 push-s10 push-m10">
                        <a href="#" class="dropdown-button btn btn-flat btn-large" data-activates="dropuser">
                            <span><i class="fas fa-user"></i> </span>
                        </a>
                        <ul id="dropuser" class="dropdown-content">
                            <li><a href="#!">${translate.message('logout')}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>`

module.exports = function header(ctx, next) {
    const container = document.getElementById('header-container')
    empty(container).appendChild(el)
    next()
}
