const page = require('page')

require('./homepage')
require('./users')
require('./signup')
require('./signin')
require('./footer')

page()