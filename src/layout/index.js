const yo = require('yo-yo')
const translate = require('../translate')

module.exports = function layout(templ) {
    return yo`<div class="content">
        ${templ}
    </div>`
}