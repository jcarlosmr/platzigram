const yo = require('yo-yo')
const translate = require('../translate')

module.exports = function () {
    return yo`<div id="loader" class="loader">${translate.message('loading')}</div>`   
}