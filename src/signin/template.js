const yo = require('yo-yo')
const landing = require('../landing')
const translate = require('../translate')

const signinForm = yo`<div class="col s12 m7">
        <div class="row">
            <div class="signup-box">
                <hi class="platzigram">Platzigram</hi>
                <form class="signup-form">
                    <div class="section">
                        <a class="btn btn-fb hide-on-small-only">${translate.message('signup.facebook')}</a>
                        <a class="btn btn-fb hide-on-med-and-up"><i class="fab fa-facebook-square"></i> ${translate.message('signup.text')}</a>
                    </div>
                    <div class="divider"></div>
                    <div class="section">
                        <input type="text" name="username" placeholder="${translate.message('username')}">
                        <input type="password" name="password" placeholder="${translate.message('password')}">
                        <button class="btn waves-effect waves-light btn-signup" type="submit">${translate.message('signin')}</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="login-box">
                ${translate.message('signin.not-have-account')} <a href="/signup">${translate.message('signup.text')}</a>
            </div>
        </div>
    </div>`

module.exports = landing(signinForm)