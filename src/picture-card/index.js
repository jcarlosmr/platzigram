const yo = require('yo-yo')
const translate = require('../translate')

module.exports = function pictureCard(pic) {
    let el
    function render(pict) {
        return yo`<div class="card ${pic.liked ? "liked" : ""}">
            <div class="card-image">
                <img class="activator" src="${pict.url}">
            </div>
            <div class="card-content">
                <a href="/user/${pict.user.username}" class="card-title">
                    <img src="${pict.user.avatar}" class="avatar" />
                    <span class="username">${pict.user.username}</span>
                </a>
                <small class="right time">${translate.date(pic.createdAt)}</small>
                <p>
                    <a href="#" class="left" onclick=${like.bind(null, true)}>
                        <div class="corazon">
                            <i class="far fa-heart ho"></i>
                        </div>
                    </a>
                    <a href="#" class="left" onclick=${like.bind(null, false)}>
                        <div class="corazon">
                            <i class="fas fa-heart hf"></i>
                        </div>
                    </a>
                    <span class="left likes">${translate.message('likes', { likes: pict.likes })}</span>
                </p>
            </div>
        </div>`
    }

    function like(liked) {
        pic.liked = liked
        pic.likes += liked ? 1 : -1
        const newEl = render(pic)
        yo.update(el, newEl)
        return false //esto evita el luego del evento onclick se coloque el # en la URL        
    }

    el = render(pic)
    return el
}