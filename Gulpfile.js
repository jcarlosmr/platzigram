const gulp = require('gulp')
const sass = require('gulp-sass')
const rename = require('gulp-rename')
const babel = require('babelify')
const browserify = require('browserify')
const source = require('vinyl-source-stream')
const watchify = require('watchify')
const watch = require('gulp-watch')
const gutil = require('gulp-util')
const livereload = require('gulp-livereload')

gulp.task('styles', styles)

gulp.task('assets', function () {
  gulp
    .src('assets/*')
    .pipe(gulp.dest('public'))
})

function styles() {
  gutil.log(gutil.colors.green('--> Building app.css'))
  return gulp
    .src('index.scss')
    .pipe(sass())
    .pipe(rename('app.css'))
    .pipe(gulp.dest('public'))
}

function compile(watch) {
  const bundle = browserify('./src/index.js')

  function rebundle() {
    bundle
      .transform(babel, {
        presets: ["es2015"]
      })
      .bundle()
      .on('error', function (err) {
        console.log(err)
        this.emit('end')
      })
      .pipe(source('index.js'))
      .pipe(rename('app.js'))
      .pipe(gulp.dest('public'))
  }

  if (watch) {
    b = watchify(bundle)
    b.on('update', function () {
      gutil.log(gutil.colors.green('--> Bundling app.js'))
      return rebundle()
    })
  }

  return rebundle()
}

gulp.task('build', function () {
  return compile()
})

gulp.task('watch', function () {
  gulp.watch('./index.scss', styles);
  return compile(true)
})

gulp.task('default', ['styles', 'assets', 'build'])