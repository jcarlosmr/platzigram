const express = require('express')
const multer  = require('multer')
const ext = require('file-extension')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.' + ext(file.originalname))
  }
})
 
const upload = multer({ storage: storage }).single('picture')

/* Eliminar despues */
const users = [
  {
    username: 'jcarlosmr',
    fullname: 'Juan C. Mendoza',
    avatar: '24279711344_6865764126_o.jpg'
  },
  {
    username: 'jose',
    fullname: 'José Peres',
    avatar: '24279711344_6865764126_o.jpg'
  },
  {
    username: 'manuel',
    fullname: 'Manuel Fernandez',
    avatar: '24279711344_6865764126_o.jpg'
  }
]

const pictures = [
  {
    user: {
      username: 'jcarlosmr',
      avatar: '24279711344_6865764126_o.jpg'
    },
    url: 'office.jpg',
    likes: 0,
    liked: false,
    createdAt: new Date().getDate()
  },
  {
    user: {
      username: 'jose',
      avatar: '24279711344_6865764126_o.jpg'
    },
    url: 'office.jpg',
    likes: 1,
    liked: true,
    createdAt: new Date().setDate(new Date().getDate() - 14)
  },
  {
    user: {
      username: 'jcarlosmr',
      avatar: '24279711344_6865764126_o.jpg'
    },
    url: 'office.jpg',
    likes: 3,
    liked: true,
    createdAt: new Date().setDate(new Date().getDate() - 5)
  }
]

/* */

const app = express()

app.set('view engine', 'pug')

app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => res.render('index', {
  title: 'Platzigram'
}))

app.get('/signup', (req, res) => res.render('index', {
  title: 'Platzigram - Signup'
}))

app.get('/signin', (req, res) => res.render('index', {
  title: 'Platzigram - Signin'
}))

app.get('/user/:username', (req, res) => res.render('index', {
  title: 'Platzigram - username',
  username: req.params.username
}))

app.get('/api/user', (req, res) => {
  res.send(users)
})

app.get('/api/user/:username', (req, res) => {
  const idx = users.findIndex((u) => u.username == req.params.username )
  res.send(users[idx])
})

app.get('/api/pictures', (req, res) => {
  res.send(pictures)
})

app.get('/api/pictures/:username', (req, res) => {
  const userPics = pictures.filter((p) => {
    return p.user.username === req.params.username
  })
  if (userPics.length > 0) {
    res.send(userPics)
  } else {
    res.send(404, 'Error 404, No picture found')
  }
})

app.post('/api/pictures', (req, res) => {
  
  upload(req, res, function (err, res) {
    if (err) return res.send(500, 'Error uploading file')
    res.send('File uploaded successfully')
  })
  
})

app.listen(3000, function (err) {
  if (err) return console.log(err), process.exit(1)
  console.log('Servidor activo en el puerto 3000')
})